import json
import requests
from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY


def get_city_photo(city, state):
    url = "https://api.pexels.com/v1/search"
    headers = {
        "Authorization": PEXELS_API_KEY,
    }
    payload = {
        "query": f"{city} {state}",
        "per_page": 1,
    }
    response = requests.get(url, params=payload, headers=headers)
    photo_dict = json.loads(response.content)
    try:
        return photo_dict["photos"][0]["url"]
    except (KeyError, IndexError):
        return None


def get_weather_data(city, state):
    query = f"{city},{state},US"
    api_key = OPEN_WEATHER_API_KEY
    geocode_url = f"http://api.openweathermap.org/geo/1.0/direct?q={query}&appid={api_key}"
    geo_response = requests.get(geocode_url)
    geocode_data = geo_response.json()
    lat = geocode_data[0]["lat"]
    lon = geocode_data[0]["lon"]
    weather_url = f"https://api.openweathermap.org/data/2.5/weather?lat={lat}&lon={lon}&appid={api_key}"
    response = requests.get(weather_url)
    weather_data = response.json()
    temp = weather_data["main"]["temp"]
    description = weather_data["weather"][0]["description"]
    weather_dict = {
        "temp": temp,
        "description": description,
    }
    return weather_dict
